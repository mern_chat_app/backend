const express = require("express");
const dotenv = require("dotenv");
const connectDB = require("./config/db");
const colors = require("colors");

dotenv.config();

connectDB();
const app = express();

app.get("/", (request, response) => {
  response.send("API IS RUNNING");
});

const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`SERVER STARTED ON PORT ${PORT}`.yellow.bold));
